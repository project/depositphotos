<?php

/**
 * @file
 * An example field using the Field Types API.
 */

/**
 * Implements hook_field_info().
 */
function depositphotos_field_info() {
  return array(
    'depositphotos_affiliate' => array(
      'label' => t('Depositphotos Affiliate'),
      'description' => t('Generate Depositphotos widget'),
      'default_widget' => 'depositphotos_affiliate_widget',
      'default_formatter' => 'depositphotos_affiliate_formatter',
    ),
  );
}

/**
 * Implements hook_field_widget_error().
 */
function depositphotos_field_widget_error($element, $error, $form, &$form_state) {
  switch ($error['error']) {
    case 'depositphotos_settings_invalid':
      form_error($element, $error['message']);
      break;
  }
}

/**
 * Implements hook_field_is_empty().
 */
function depositphotos_field_is_empty($item, $field) {
  return empty($item['settings']);
}

/**
 * Implements hook_field_presave().
 */
function depositphotos_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {
  if ($field['type'] == 'depositphotos_affiliate') {
    foreach ($items as $delta => $item) {
      if (isset($item['settings'])) {
        $items[$delta]['settings'] = json_encode($item['settings']);
      }
    }
  }
}

/**
 * Implements hook_field_formatter_info().
 */
function depositphotos_field_formatter_info() {
  return array(
    // This formatter changes the background color of the content region.
    'depositphotos_affiliate_formatter' => array(
      'label' => t('Display Depositphotos Affiliate widget block.'),
      'field types' => array('depositphotos_affiliate'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function depositphotos_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  global $language;

  $element = array();

  switch ($display['type']) {
    case 'depositphotos_affiliate_formatter':
      foreach ($items as $delta => $item) {
        if (!empty($item['settings'])) {
          $settings = drupal_json_decode($item['settings']);

          $settings['lang'] = $language->language;
          $settings['wid'] = 44;

          // Move settings from fieldset to root of array.
          $fieldsets = array(
            'feed',
            'thumbs',
            'appearance',
            'feedsize',
            'type',
          );
          foreach ($fieldsets as $fieldset) {
            $settings = array_merge($settings, $settings[$fieldset]);
            unset($settings[$fieldset]);
          }

          // Formatting checkboxes in the format that is suitable
          // for Depositphotos.
          $checkboxes = array(
            'photo',
            'vector',
            'video',
            'editorial',
            'nofollow',
          );
          foreach ($checkboxes as $checkbox) {
            $settings[$checkbox] = $settings[$checkbox] ? 'true' : 'false';
          }

          // Unset empty settings or with value "false".
          foreach ($settings as $key => $setting) {
            if (empty($setting) || $setting == 'false') {
              unset($settings[$key]);
            }
          }

          switch ($settings['format']) {
            case 'iframe':
              $prepared_settings = drupal_json_encode($settings);

              $element[$delta] = array(
                '#type' => 'html_tag',
                '#tag' => 'iframe',
                '#value' => '',
                '#attributes' => array(
                  'class' => array('dp-widget-frame'),
                  'style' => 'display: none;',
                  'data-dpw' => 44,
                  'src' => 'javascript:void(false)',
                  'width' => DEPOSITPHOTOS_DEFAULT_FEED_WIDTH,
                  'height' => DEPOSITPHOTOS_DEFAULT_FEED_HEIGHT,
                ),
                '#attached' => array(
                  'js' => array(
                    array(
                      'data' => '//static.depositphotos.com/js_c/widget-ext.js',
                      'type' => 'external',
                      'scope' => 'footer',
                    ),
                    array(
                      'data' => "(function(){new dpw({$prepared_settings}).init(); })();",
                      'type' => 'inline',
                      'scope' => 'footer',
                    ),
                  ),
                ),
              );
              break;

            case 'div':
              $prepared_settings = drupal_json_encode($settings);

              $element[$delta] = array(
                '#type' => 'html_tag',
                '#tag' => 'div',
                '#value' => '',
                '#attributes' => array(
                  'class' => array('dp-widget'),
                  'data-dpw' => $settings['wid'],
                ),
                '#attached' => array(
                  'js' => array(
                    array(
                      'data' => '//static.depositphotos.com/js_c/widget-ext.js',
                      'type' => 'external',
                      'scope' => 'footer',
                    ),
                    array(
                      'data' => "(function(){new dpw({$prepared_settings}).init(); })();",
                      'type' => 'inline',
                      'scope' => 'footer',
                    ),
                  ),
                ),
              );
              break;

            case 'php':
            default:
              $settings['format'] = 'div';
              $prepared_settings = drupal_json_encode($settings);

              $params = array(
                'dp_apikey' => DEPOSITPHOTOS_API_KEY,
                'dp_command' => DEPOSITPHOTOS_COMMAND_GET_WIDGET,
                'dp_widget_config' => $prepared_settings,
                'dp_widget_referer' => drupal_json_encode($_GET),
              );
              $query = drupal_http_build_query($params);
              $widget_content = file_get_contents(DEPOSITPHOTOS_API_BASE_PATH . '?' . $query);

              $element[$delta] = array(
                '#type' => 'html_tag',
                '#tag' => 'div',
                '#value' => $widget_content,
              );
              break;
          }
        }
      }
      break;
  }

  return $element;
}

/**
 * Implements hook_field_widget_info().
 */
function depositphotos_field_widget_info() {
  return array(
    'depositphotos_affiliate_widget' => array(
      'label' => t('Affiliate block'),
      'field types' => array('depositphotos_affiliate'),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function depositphotos_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $value = isset($items[$delta]) ? $items[$delta] : array();

  $widget = $element;
  // #delta is set so that the validation function will be able
  // to access external value information which otherwise would be
  // unavailable.
  $widget['#delta'] = $delta;

  switch ($instance['widget']['type']) {
    case 'depositphotos_affiliate_widget':
      if (isset($value['settings']) && is_string($value['settings'])) {
        $settings = drupal_json_decode($value['settings']);
      }
      elseif (isset($value['settings'])) {
        $settings = $value['settings'];
      }
      else {
        $settings = array();
      }

      // Make widget a fieldset.
      $widget += array(
        '#type' => 'fieldset',
        '#attached' => array(
          'css' => array(drupal_get_path('module', 'depositphotos') . '/depositphotos.css'),
        ),
        '#element_validate' => array('depositphotos_affiliate_widget_validate'),
      );
      $widget['format'] = array(
        '#type' => 'radios',
        '#title' => t('Format widget'),
        '#default_value' => !empty($settings['format']) ? $settings['format'] : variable_get('depositphotos_widget_format', 'iframe'),
        '#options' => array(
          'iframe' => t('Iframe'),
          'php' => t('PHP'),
          'div' => t('Block'),
        ),
      );
      $widget['trackingLink'] = array(
        '#type' => 'textfield',
        '#title' => t('Tracking link'),
        '#description' => t('Please enter your tracking link here. You will receive your personal tracking link once registered in the Affiliate Program.<br /><em>Example: http://tracking.depositphotos.com/aff_c?offer_id=4&aff_id=1560</em><br />In order to copy your link, please open the offer front page and find the link in your account:'),
        '#required' => TRUE,
        '#default_value' => !empty($settings['trackingLink']) ? $settings['trackingLink'] : variable_get('depositphotos_affiliate_url', ''),
      );
      $widget['feed'] = array(
        '#type' => 'fieldset',
        '#title' => t('Feed settings'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );
      $widget['feed']['feedtype'] = array(
        '#type' => 'select',
        '#title' => t('Feed type'),
        '#description' => t('Please select one of the three feed types: Category, Portfolio, Search.'),
        '#default_value' => !empty($settings['feed']['feedtype']) ? $settings['feed']['feedtype'] : 'category',
        '#options' => array(
          'categories' => t('Category'),
          'user' => t('Portfolio'),
          'search' => t('Search'),
        ),
        '#attributes' => array(
          'data-format' => '',
        ),
      );
      $widget['feed']['categorylist'] = array(
        '#type' => 'select',
        '#title' => t('Category list'),
        '#default_value' => !empty($settings['feed']['categorylist']) ? $settings['feed']['categorylist'] : DEPOSITPHOTOS_DEFAULT_FEED_CATEGORY_LIST,
        '#options' => depositphotos_affiliate_widget_categories_options(),
        '#states' => array(
          'visible' => array(
            '[data-format]' => array('value' => 'categories'),
          ),
        ),
      );
      $widget['feed']['searchquery'] = array(
        '#type' => 'textfield',
        '#title' => t('Search query'),
        '#default_value' => !empty($settings['feed']['searchquery']) ? $settings['feed']['searchquery'] : '',
        '#states' => array(
          'visible' => array(
            '[data-format]' => array('value' => 'search'),
          ),
        ),
      );
      $widget['feed']['authorid'] = array(
        '#type' => 'textfield',
        '#title' => t('Author ID / Login'),
        '#default_value' => !empty($settings['feed']['authorid']) ? $settings['feed']['authorid'] : '',
        '#states' => array(
          'visible' => array(
            '[data-format]' => array('value' => 'user'),
          ),
        ),
      );
      $widget['feed']['type'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Type'),
        '#options' => array(
          'photo' => t('Photo'),
          'vector' => t('Vector'),
          'video' => t('Video'),
        ),
        '#default_value' => !empty($settings['feed']['type']) ? $settings['feed']['type'] : array(
          'photo',
          'vector',
          'video',
        ),
      );
      $widget['feed']['editorial'] = array(
        '#type' => 'checkbox',
        '#title' => t('Editorial only'),
        '#description' => t('Please choose "Editorial only" if you would like to display only editorial files.<br />Note that even if you choose other types of files above (like photo, vector image or video), the selection of "Editorial only" will restrict files displayed in the widget to just those for editorial use.'),
        '#default_value' => !empty($settings['feed']['editorial']) ? $settings['feed']['editorial'] : FALSE,
      );
      $widget['feed']['sort'] = array(
        '#type' => 'select',
        '#title' => t('Sort by'),
        '#description' => t('With our widget, you can sort to see files recently added or most popular. If you want the files to be displayed at random, select the Random parameter.'),
        '#default_value' => !empty($settings['feed']['sort']) ? $settings['feed']['sort'] : 1,
        '#options' => array(
          5 => t('Newest'),
          1 => t('Popular'),
          'random' => t('Random'),
        ),
      );
      $widget['feed']['feedsize'] = array(
        '#type' => 'fieldset',
        '#title' => t('Feed size'),
        '#description' => t('Please enter the dimensions of the feed.<br />Note that the minimum width is 130px, and the minimum height is 125px (appropriate if you choose to display the widget with icons only; without a logo, a search bar or a pager).'),
      );
      $widget['feed']['feedsize']['feedwidth'] = array(
        '#type' => 'textfield',
        '#title' => t('Width'),
        '#required' => TRUE,
        '#default_value' => !empty($settings['feed']['feedsize']['feedwidth']) ? $settings['feed']['feedsize']['feedwidth'] : DEPOSITPHOTOS_DEFAULT_FEED_WIDTH,
      );
      $widget['feed']['feedsize']['feedheight'] = array(
        '#type' => 'textfield',
        '#title' => t('Height'),
        '#required' => TRUE,
        '#default_value' => !empty($settings['feed']['feedsize']['feedheight']) ? $settings['feed']['feedsize']['feedheight'] : DEPOSITPHOTOS_DEFAULT_FEED_HEIGHT,
      );
      $widget['thumbs'] = array(
        '#type' => 'fieldset',
        '#title' => t('Thumbnail settings'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );
      $widget['thumbs']['thumbsize'] = array(
        '#type' => 'select',
        '#title' => t('Thumbnail size'),
        '#description' => t('Please choose the appropriate thumbnail size for your widget; thumbnails shouldn’t be too small or too large.'),
        '#options' => array(
          110 => t('110px'),
          170 => t('170px'),
          'specify_size' => t('Specify size'),
        ),
        '#default_value' => !empty($settings['thumbs']['thumbsize']) ? $settings['thumbs']['thumbsize'] : DEPOSITPHOTOS_DEFAULT_THUMBSIZE,
        '#attributes' => array(
          'data-thumbsize' => '',
        ),
      );
      $widget['thumbs']['specify_size'] = array(
        '#type' => 'textfield',
        '#title' => t('Select thumbnail size'),
        '#default_value' => !empty($settings['thumbs']['specify_size']) ? $settings['thumbs']['specify_size'] : '',
        '#states' => array(
          'visible' => array(
            '[data-thumbsize]' => array('value' => 'specify_size'),
          ),
          'required' => array(
            '[data-thumbsize]' => array('value' => 'specify_size'),
          ),
        ),
      );
      $widget['thumbs']['preview'] = array(
        '#type' => 'radios',
        '#title' => t('Thumbnails preview'),
        '#description' => t('Please choose “Yes,” if you would like to show an image preview when the mouse rolls over the thumbnail.'),
        '#default_value' => !empty($settings['thumbs']['preview']) ? $settings['thumbs']['preview'] : 'true',
        '#options' => array(
          'false' => t('No'),
          'true' => t('Yes'),
        ),
      );
      $widget['appearance'] = array(
        '#type' => 'fieldset',
        '#title' => t('Appearance settings'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );
      $widget['appearance']['theme'] = array(
        '#type' => 'select',
        '#title' => t('Theme'),
        '#default_value' => !empty($settings['appearance']['theme']) ? $settings['appearance']['theme'] : 'dark',
        '#options' => array(
          'false' => t('Light'),
          'dark' => t('Dark'),
          'white' => t('White'),
          'transparent' => t('Transparent'),
        ),
      );
      $widget['appearance']['background'] = array(
        '#type' => 'select',
        '#title' => t('Background'),
        '#default_value' => !empty($settings['appearance']['background']) ? $settings['appearance']['background'] : 'light',
        '#options' => array(
          'false' => t('Light'),
          'dark' => t('Dark'),
          'transparent' => t('Transparent'),
        ),
      );
      $widget['appearance']['searchBar'] = array(
        '#type' => 'radios',
        '#title' => t('Search bar'),
        '#description' => t('Please choose "Yes", if you would like to add the search bar to your widget.'),
        '#default_value' => !empty($settings['appearance']['searchBar']) ? $settings['appearance']['searchBar'] : 'true',
        '#options' => array(
          'false' => t('No'),
          'true' => t('Yes'),
        ),
      );
      $widget['appearance']['showlogo'] = array(
        '#type' => 'radios',
        '#title' => t('Show logo'),
        '#description' => t('Please choose whether your would like to display the Depositphotos logo in your widget'),
        '#default_value' => !empty($settings['appearance']['showlogo']) ? $settings['appearance']['showlogo'] : 'true',
        '#options' => array(
          'false' => t('No'),
          'true' => t('Yes'),
        ),
      );
      $widget['appearance']['limitpage'] = array(
        '#type' => 'radios',
        '#title' => t('Hide pager'),
        '#default_value' => !empty($settings['appearance']['limitpage']) ? $settings['appearance']['limitpage'] : 'false',
        '#options' => array(
          'false' => t('No'),
          'true' => t('Yes'),
        ),
      );
      $widget['appearance']['showborder'] = array(
        '#type' => 'radios',
        '#title' => t('Show border'),
        '#description' => t('Please choose whether you would like to display or hide the borders of your widget.'),
        '#default_value' => !empty($settings['appearance']['showborder']) ? $settings['appearance']['showborder'] : 'true',
        '#options' => array(
          'false' => t('No'),
          'true' => t('Yes'),
        ),
      );
      $widget['appearance']['responsive'] = array(
        '#type' => 'radios',
        '#title' => t('Responsive mode'),
        '#description' => t("Please choose \"Yes\" if you want to make your widget sensitive to the changes in your browser's window size."),
        '#default_value' => !empty($settings['appearance']['responsive']) ? $settings['appearance']['responsive'] : 'false',
        '#options' => array(
          'false' => t('No'),
          'true' => t('Yes'),
        ),
      );
      $widget['nofollow'] = array(
        '#type' => 'checkbox',
        '#title' => t('Add rel="nofollow" to a search result hyperlinks'),
        '#default_value' => !empty($settings['nofollow']) ? $settings['nofollow'] : FALSE,
      );

      break;
  }

  $element['settings'] = $widget;

  return $element;
}

/**
 * Validate the individual fields.
 */
function depositphotos_affiliate_widget_validate($element, &$form_state) {
  $delta = $element['#delta'];
  $field = $form_state['field'][$element['#field_name']][$element['#language']]['field'];
  $field_name = $field['field_name'];
  if (isset($form_state['values'][$field_name][$element['#language']][$delta]['settings'])) {
    $settings = $form_state['values'][$field_name][$element['#language']][$delta]['settings'];

    // Check width of the feed.
    if (isset($settings['feed']['feedsize']['feedwidth'])) {
      if ($settings['feed']['feedsize']['feedwidth'] < DEPOSITPHOTOS_MINIMUM_FEED_WIDTH) {
        form_error($element['feed']['feedsize']['feedwidth'], t('Minimum width of the feed is 130px'));
      }
    }
    else {
      form_error($element['feed']['feedsize']['feedwidth'], t('Width of the feed should be integer value'));
    }

    // Check height of the feed.
    if (isset($settings['feed']['feedsize']['feedheight'])) {
      if ($settings['feed']['feedsize']['feedheight'] < DEPOSITPHOTOS_MINIMUM_FEED_HEIGHT) {
        form_error($element['feed']['feedsize']['feedheight'], t('Minimum height of the feed is 125px'));
      }
    }
    else {
      form_error($element['feed']['feedsize']['feedheight'], t('Height of the feed should be integer value'));
    }

    if (isset($settings['thumbs']['thumbsize']) && $settings['thumbs']['thumbsize'] == 'specify_size' && empty($settings['thumbs']['specify_size'])) {
      form_error($element['thumbs']['specify_size'], t('"Select thumbnail size" field is required if selected type of Thumbnail size is "Specify size"'));
    }
  }
}
