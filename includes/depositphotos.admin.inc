<?php

/**
 * @file
 * Administration forms.
 */

/**
 * Form builder for a form to configure Depositphotos module.
 */
function depositphotos_settings_form($form, $form_state) {
  $form['widget_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Widget settings'),
  );

  $form['widget_settings']['depositphotos_affiliate_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Affiliate URL'),
    '#default_value' => variable_get('depositphotos_affiliate_url', ''),
  );

  $form['widget_settings']['depositphotos_widget_format'] = array(
    '#type' => 'radios',
    '#title' => t('Format widget'),
    '#options' => array(
      'iframe' => t('iframe'),
      'div' => t('Block'),
      'php' => t('PHP'),
    ),
    '#default_value' => variable_get('depositphotos_widget_format', 'iframe'),
  );

  $form['faq'] = array(
    '#type' => 'markup',
    '#title' => t('Test'),
    '#markup' => t("To use the module, you need to get a tracking code from the Depositphotos affiliate program. <br />
Register on this link: https://depositphotos.com/affiliate/signup.html<br />
After registration, you will receive a confirmation with a unique code that looks like this: tracking.depositphotos.com/aff + unique identifiers<br />
Install module correctly with the use of keywords or categories so that your photographs will be thematically fitting to your publication."),
  );

  return system_settings_form($form);
}
