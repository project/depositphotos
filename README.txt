CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------

This module gives you the opportunity to use and publish illustrations with
affiliate links from Depositphotos, one of the leading photobanks in the world.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/depositphotos

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2856589

REQUIREMENTS
------------

For using the plugin, you will need a Depositphotos affiliate account.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * Go to module settings page /admin/modules and enable Depositphotos module.

CONFIGURATION
-------------

 * Go to Depositphotos settings page /admin/config/services/depositphotos
   and fill there Affiliate url and default format of widget.

 * Save configuration.

 * Configure new field with type "Depositphotos Affiliate" at this page
   /admin/structure/types/manage/[NODE_TYPE]/fields

 * When creating node, fill in the required information in a new field
   and save node.
